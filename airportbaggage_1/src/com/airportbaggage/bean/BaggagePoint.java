package com.airportbaggage.bean;

/**
 * A baggage point is a zone identified by (and part of) the conveyor system. A
 * baggage point can act as source / destination / intermediate zone for
 * checked-in bags in the conveyor system.
 * 
 * 
 */
public interface BaggagePoint extends Zone {

}