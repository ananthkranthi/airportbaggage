package com.airportbaggage.bean;

/**
 * A gate is a baggage point that acts as the connection between an airport and
 * a scheduled flight.
 * 
 */
public interface Gate extends BaggagePoint {

}